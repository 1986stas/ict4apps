Feature: In this feature I'm testing welcome page

  Scenario Outline: I'm testing site navigation menu on welcome page
    Given Get "http://192.168.214.3:8080/welcome"
    When Click on <item> in site navigation menu
    Examples:
    |item|
    |WELCOME|
    |PRODUCTS|
    |SPECIAL OFFERS|
    |BLOGS|
    |CONTACT US|


  Scenario: I'm testing possibility to download App "ict4apps" from AppStore, GooglePlay
    Given Get "http://192.168.214.3:8080/welcome"
    When Click on link AppSTore
    And Click on link GooglePlay
    Then The Welcome page should be opened

  Scenario: I'm testing link "Title of products" on first images on "WELCOME" page
    Given Get "http://192.168.214.3:8080/welcome"
    When Check carousel-button "Next"
    And Click on carousel-button "Previous"
    And Click on first images
    Then The chosen product of "SPECIAL OFFERS" box should be opened

  Scenario: I'm testing link "Title of products" on last images on "WELCOME" pag
    Given Get "http://192.168.214.3:8080/welcome"
    When Click on last images
    Then The chosen product of "SPECIAL OFFERS" box should be opened

  Scenario: I'm testing first link "FOOD DELIVERY BOXES" on welcome page
    Given Get "http://192.168.214.3:8080/welcome"
    When Click on first images in FOOD DELIVERY BOXES
    Then The chosen product FOOD DELIVERY BOXES should be opened

  Scenario: I'm testing last link "FOOD DELIVERY BOXES" on welcome page
    Given Get "http://192.168.214.3:8080/welcome"
    When Click on last images in FOOD DELIVERY BOXES
    Then The chosen product FOOD DELIVERY BOXES should be opened

  Scenario: I'm testing button "next" and "previous" "FOOD DELIVERY BOXES" on welcome page
    Given Get "http://192.168.214.3:8080/welcome"
    When Click on button next
    And Click on button previous
    Then Images with products should be changed

  Scenario: I'm testing link "Read more about us" "FOOD DELIVERY BOXES" on welcome page
    Given Get "http://192.168.214.3:8080/welcome"
    When Click on link ReadMoreAboutUs
    Then Should be opening category "Read more about us"

  Scenario: I'm testing search query with "SEARCH" button
    Given Get "http://192.168.214.3:8080/welcome"
    When Input complete request "apple" in search query
    Then The result of request in document search container should be appeared

  Scenario Outline: I'm testing "Select scope" on search page
    Given Go to "http://192.168.214.3:8080/search"
    When Enter complete request "apple" in search query
    And In <selected scope> select global
    Then The result of request in document search container should be appeared
    Examples:
      | selected scope |
      |global          |
      |blogs           |

  Scenario: I'm testing box "BLOGS" on "WELCOME" page
    Given Go to "http://192.168.214.3:8080/welcome"
    When Click on heading on first news in blog-list
    And Click on link Read more
    Then Should be opened website, there located chosen post

  Scenario: I'm testing links on twitter in box "BLOGS" on "WELCOME" page
    Given Go to "http://192.168.214.3:8080/welcome"
    Given The first post in BLOGS on WELCOME page is opened
    When Click on Twitter link

  Scenario Outline: I'm testing footer navigation menu on welcome page
    Given Get "http://192.168.214.3:8080/welcome"
    When Click on <footer item> in footer
    Examples:
      |footer item|
      |WELCOME|
      |PRODUCTS|
      |SPECIAL OFFERS|
      |BLOGS|
      |CONTACT US|

  Scenario: I'm testing "Back top" button
      Given Go to "http://192.168.214.3:8080/welcome"
      When Scroll down to the footer and click on back top button
      Then The top of the page should be appeared

  Scenario: I'm testing first phone number link on welcome page
    Given Go to "http://192.168.214.3:8080/welcome"
    When Click on the first phone number link
    Then The special page with telephone number should be opened
























