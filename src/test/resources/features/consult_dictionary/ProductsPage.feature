Feature: In this feature I'm testing Products page in site navigation

  Scenario: Check drop down menu "PRODUCTS"
    Given Go to url  "http://192.168.214.3:8080/welcome"
    When Click on Products in site navigation menu in header
    Then The chosen item should be opened

  Scenario: Check "FRUITS" in drop down menu
    Given Go to url  "http://192.168.214.3:8080/products"
    Given Page "PRODUCTS" opened
    When Click on Fruits in drop down menu
    And Click on Vegetables
    Then The chosen category of FRUITS should be opened

  Scenario: Check link "READ MORE"
    Given Category Vegetables opened
    When Click on link "READ MORE"
    Then The chosen product should be opened

  Scenario: Сheck possibility to enlarge image size
    Given Go to url  "http://192.168.214.3:8080/products/-/product/category/vegetables/get-entity/113"
    Given Product page is opened
    When Click on image of product
    Then Click on close button

  Scenario: Check tags list Vegetables
    Given Go to url  "http://192.168.214.3:8080/products/-/product/category/vegetables/get-entity/113"
    Given Product page is opened
    When Click on item "Vegetables"
    Then Previous levels must be opened

  Scenario: Check heading of product
    Given  Go to url  "http://192.168.214.3:8080/products/-/category/vegetables"
    When Click on heading of product
    Then The chosen product should be opened

  Scenario: Check breadcrumb
    Given Go to url  "http://192.168.214.3:8080/products/-/product/category/vegetables/get-entity/113"
    When Click on item "VEGETABLES" on breadcrumb
    Then Previous levels must be opened

  Scenario: Check "Grid list" and "View list" button
    Given Go to url  "http://192.168.214.3:8080/products/-/category/vegetables"
    When Click on "Grid list"
    And Click on View list
    Then The element should be presented

  Scenario: I'm testing pagination button on "PRODUCTS" page above products list
    Given Go to "http://192.168.214.3:8080/products"
    When Click on pagination button last
    And Click on pagination button first
    And Click on pagination button next
    And Click on pagination button previous
    Then The message appeared

  Scenario Outline: I'm testing social net
    Given Go to url  "http://192.168.214.3:8080/products/-/category/vegetables"
    When Click on <social net> <url>
    Examples:
    |social net|url|
    |VKontakte |vk.com|
    |Facebook  |facebook.com|
    |Twitter   |twitter.com
    |LinkedIn  |linkedin.com|
    |Pinterest |pinterest.com|
    |Google Plus  |google.com |

  Scenario: I'm testing links on social network in footer on product page
    Given


