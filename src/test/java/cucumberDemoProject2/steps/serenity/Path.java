package cucumberDemoProject2.steps.serenity;

/**
 * Created by user on 10.05.16.
 */
public interface Path {
    String ITEMS_OF_SITE_NAVIGATION_MENU = ".//*/nav/ul/li/a/span";
    String ITEM_IS_PRESENT = ".//*/nav/ul/li/a/span";
    String THE_PRODUCT_SHOULD_BE_CHANGED = "//div[@data-slick-index='4']";
    String LINK_READ_MORE_ABOUT_US = "//div[1]/div/a[@href='/about-us']";
    String LINK_SHOULD_BE_CONTAINS = "//span[@class='portlet-title-text']";
    String CLICK_ON_FIRST_NEWS = ".//div[3]/h3/a";
    String CLICK_ON_READ_MORE = "//div/a[contains(text(),'Read More')]";
    String CLICK_ON_TWITTER_LINK = "//div/ul/li/iframe[@id='twitter-widget-0']";
    String ELEMENT_IS_PRESENT = ".//*/nav/ul/li/a/span";
    String CLICK_ON_NUMBER = "//div//a[@href='tel:12345678901']";
    String PHONE_NUMBER_IS_ENABLED = "//div//a[@href='tel:12345678901']";


}
