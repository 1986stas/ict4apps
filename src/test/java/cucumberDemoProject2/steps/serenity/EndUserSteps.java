package cucumberDemoProject2.steps.serenity;

import cucumberDemoProject2.pages.DictionaryPage;
import cucumberDemoProject2.pages.ProductPage;
import cucumberDemoProject2.pages.SystemRegistration;
import cucumberDemoProject2.pages.WelcomePage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.junit.Assert;

import java.awt.*;

public class EndUserSteps extends ScenarioSteps {

    DictionaryPage dictionaryPage;
    ProductPage productPage;
    SystemRegistration systemRegistration;
    WelcomePage welcomePage;


    @Step
    public void goToUrl(String arg0) {
        getDriver().get(arg0);
    }

    @Step
    public void clickOnProductsInSiteNavigationMenuInHeader() {
        productPage.clickOnProductsInSiteNavigationMenuInHeader();
    }

    @Step
    public void theChosenItemShouldBeOpened() {

        Assert.assertTrue("False", productPage.theChosenItemShouldBeOpened());
    }

    @Step
    public void pageOpened() {

        Assert.assertTrue("False", productPage.pageOpened());
    }

    @Step
    public void clickOnFruitsInDropDownMenu() {

        productPage.clickOnFruitsInDropDownMenu();
    }

    @Step
    public void clickOnVegetables() {

        productPage.clickOnVegetables();
    }

    @Step
    public void theChosenCategoryOfFRUITSShouldBeOpened() {
        Assert.assertTrue("False", productPage.theChosenCategoryOfFRUITSShouldBeOpened());
    }

    @Step
    public void categoryVegetablesOpened() {
        Assert.assertTrue("False", productPage.categoryVegetablesOpened());
    }

    @Step
    public void clickOnLink() {
        productPage.clickOnLink();
    }

    @Step
    public void theChosenProductShouldBeOpened() {
        Assert.assertTrue("False", productPage.theChosenProductShouldBeOpened());
    }

    @Step
    public void productPageIsOpened() {
        productPage.productPageIsOpened();
    }

    @Step
    public void clickOnImageOfProduct() {
        productPage.clickOnImageOfProduct();
    }

    @Step
    public void clickOnCloseButton() {
        productPage.clickOnCloseButton();
    }

    @Step
    public void clickOnItem() {
        productPage.clickOnItem();
    }

    @Step
    public void previousLevelsMustBeOpened() {
        Assert.assertTrue("False", productPage.previousLevelsMustBeOpened());
    }

    @Step
    public void clickOnHeadingOfProduct() {
        productPage.clickOnHeadingOfProduct();
    }

    @Step
    public void clickOnItemOnBreadcrumb() {
        productPage.clickOnItemOnBreadcrumb();
    }

    @Step
    public void clickOn() {
        productPage.clickOn();
    }

    @Step
    public void clickOnViewList() {
        productPage.clickOnViewList();
    }

    @Step
    public void theElementShouldBePresented() {
        productPage.theElementShouldBePresented();
    }

    @Step
    public void goTo(String arg0) {
        getDriver().get(arg0);

    }

    @Step
    public void clickOnSINGIN() {
        systemRegistration.clickOnSINGIN();
    }

    @Step
    public void clickOnLinkCreateAccount() {
        systemRegistration.clickOnLinkCreateAccount();
    }

    @Step
    public void clickOnButtonSAVE() {
        systemRegistration.clickOnButtonSAVE();
    }

    @Step
    public void theMessagesShouldBeAppeared() {
        Assert.assertTrue("False", systemRegistration.theMessagesShouldBeAppeared());
    }

    @Step
    public void theMessageShouldBeAppeared() {
        Assert.assertTrue("False", systemRegistration.theMessageShouldBeAppeared());
    }

    @Step
    public void get(String welcomePageOpened) {
        getDriver().manage().window();
        welcomePage.open();
    }

    @Step
    public void clickOnItemInSiteNavigationMenu(String clickOnItem) throws InterruptedException {
        welcomePage.clickOnItemInSiteNavigationMenu(clickOnItem);
    }


    @Step
    public void entryFieldEmailAddressEmpty() {
        Assert.assertTrue("Not Empty", systemRegistration.entryFieldEmailAddressEmpty());
    }

    @Step
    public void inputValuesOnEntryFieldEmailAddress() {
        systemRegistration.inputValuesOnEntryFieldEmailAddress();
    }

    @Step
    public void clickOnLinkAppSTore() throws AWTException {
        welcomePage.clickOnLinkAppSTore();
    }
    @Step
    public void clickOnLinkGooglePlay() {
        welcomePage.clickOnLinkGooglePlay();
    }
    @Step
    public void theWelcomePageShouldBeOpened() {
        welcomePage.theWelcomePageShouldBeOpened();
    }

    @Step
    public void checkCarouselButton() throws InterruptedException {
        welcomePage.checkCarouselButton();
    }

    @Step
    public void clickOnCarouselButton() {
        welcomePage.clickOnCarouselButton();

    }
    @Step
    public void clickOnFirstImages() throws InterruptedException, AWTException {
        welcomePage.clickOnFirstImages();
    }
    @Step
    public boolean theChosenProductOfBoxShouldBeOpened(String arg0) {
        welcomePage.theChosenProductOfBoxShouldBeOpened(arg0);
        return true;
    }
    @Step
    public void clickOnLastImages() throws AWTException {
        welcomePage.clickOnLastImages();
    }
    @Step
    public void clickOnFirstImagesInFOODDELIVERYBOXES()  {
        welcomePage.clickOnFirstImagesInFOODDELIVERYBOXES();
    }
    @Step
    public void theChosenProductFOODDELIVERYBOXESShouldBeOpened() {
        welcomePage.theChosenProductFOODDELIVERYBOXESShouldBeOpened();
    }
    @Step
    public void clickOnLastImagesInFOODDELIVERYBOXES()  {
        welcomePage.clickOnLastImagesInFOODDELIVERYBOXES();
    }
    @Step
    public void clickOnButtonNext() {
        welcomePage.clickOnButtonNext();
    }
    @Step
    public void clickOnButtonPrevious() {
        welcomePage.clickOnButtonPrevious();
    }
    @Step
    public void imagesWithProductsShouldBeChanged() {
        welcomePage.imagesWithProductsShouldBeChanged();
    }
    @Step
    public void clickOnLinkReadMoreAboutUs() {
        welcomePage.clickOnLinkReadMoreAboutUs();
    }
    @Step
    public void shouldBeOpeningCategory(String readMoreAboutUs) {
        welcomePage.shouldBeOpeningCategory(readMoreAboutUs);
    }
    @Step
    public void inputCompleteRequestInSearchQuery(String apple) {
        welcomePage.inputCompleteRequestInSearchQuery(apple);
    }
    @Step
    public void theResultOfRequestInDocumentSearchContainerShouldBeAppeared() {
        welcomePage.theResultOfRequestInDocumentSearchContainerShouldBeAppeared();
    }

    @Step
    public void inSelectedScopeSelectGlobal(String global) {
        welcomePage.inSelectedScopeSelectGlobal(global);
    }
    @Step
    public void enterCompleteRequestInSearchQuery() {
        welcomePage.enterCompleteRequestInSearchQuery();
    }
    @Step
    public void clickOnHeadingOnFirstNewsInBlogList() {
        welcomePage.clickOnHeadingOnFirstNewsInBlogList();
    }
    @Step
    public void clickOnLinkReadMore() {
        welcomePage.clickOnLinkReadMore();
    }
    @Step
    public void shouldBeOpenedWebsiteThereLocatedChosenPost() {
        welcomePage.shouldBeOpenedWebsiteThereLocatedChosenPost();
    }
    @Step
    public void theFirstPostInBLOGSOnWELCOMEPageIsOpened() {
        welcomePage.theFirstPostInBLOGSOnWELCOMEPageIsOpened();
    }
    @Step
    public void clickOnTwitterLink() throws AWTException {
        welcomePage.clickOnTwitterLink();
    }
    @Step
    public void clickOnFooterItemInFooter(String clickOnItem) {
        welcomePage.clickOnFooterItemInFooter(clickOnItem);
    }
    @Step
    public void scrollDownToTheFooterAndClickOnBackTopButton() {
        welcomePage.scrollDownToTheFooterAndClickOnBackTopButton();
    }
    @Step
    public void theTopOfThePageShouldBeAppeared() {
        welcomePage.theTopOfThePageShouldBeAppeared();
    }
    @Step
    public void clickOnTheFirstPhoneNumberLink() {
        welcomePage.clickOnTheFirstPhoneNumberLink();
    }
    @Step
    public void theSpecialPageWithTelephoneNumberShouldBeOpened() {
        welcomePage.theSpecialPageWithTelephoneNumberShouldBeOpened();
    }
    @Step
    public void clickOnPaginationButtonLast() {
        productPage.clickOnPaginationButtonLast();
    }
    @Step
    public void clickOnPaginationButtonFirst() {
        productPage.clickOnPaginationButtonFirst();
    }
    @Step
    public void clickOnPaginationButtonNext() {
        productPage.clickOnPaginationButtonNext();
    }
    @Step
    public void clickOnPaginationButtonPrevious() {
        productPage.clickOnPaginationButtonPrevious();
    }
    @Step
    public void theMessageAppeared() {
        productPage.theMessageAppeared();
    }
    @Step
    public void clickOnSocialNet(String socialNet) throws InterruptedException {
        productPage.clickOnSocialNet(socialNet);
    }

}



  //  @Step
  //  public void clickOnItemOnTheNavigationMenu(String arg0) {
  //     List<WebElementFacade> navMenuItemsList = findAll(ILocators.WELCOME_HEADER_LINKS_ELEMENTS);
 //       for (WebElementFacade webElementFacade : navMenuItemsList) {
  //          if (webElementFacade.getText().equalsIgnoreCase(arg0)) {
  //              webElementFacade.click();
  //              return;
  //          }
  //      }
  //      throw new NoSuchElementException("Element " + arg0 + " not found in Navigation menu");
 //   }

