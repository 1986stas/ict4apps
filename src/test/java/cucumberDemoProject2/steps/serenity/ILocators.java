package cucumberDemoProject2.steps.serenity;

/**
 * Created by user on 28.04.16.
 */

/**
 * LOCATORS FOR PRODUCT PAGE (ProductPage.java)
 */
public interface ILocators {
    String CLICK_ON_ITEM_PRODUCTS = "(//span[contains(text(),'Products')])[1]";
    String CLICK_ON_FRUITS_IN_SITE_NAVIGATION_MENU = "(//a[contains(text(),'Fruits')])[3]/../../a[1]";
    String CLICK_ON_READMORE = "//a[@class='product-list__readmore product-readmore__link']";
    String PRODUCTS_HEADER_TITLE = "//h3/span[contains(text(),'Products')]";
    String ITEM_PRODUCTS_OPENED = "//a[@href='http://192.168.214.3:8080/products']";
    String CLICK_ON_VEGETABLES = "//a[.='Vegetables']";
    String ITEM_VEGETABLES_SHOULD_BE_OPENED = "//h3/span[contains(text(),'Vegetables')]";
    String CATEGORY_VEGETABLES_OPENED = "//h3/span[contains(text(),'Vegetables')]";
    String CHOSEN_PRODUCT_SHOULD_BE_OPENED = "//a[@class='icon-circle-arrow-left previous-level']";
    String CLICK_ON_IMAGE = "//img[@class]";
    String CLICK_ON_CLOSE_BUTTON = "//button[@class='image-viewer-close close']";
    String PRODUCT_PAGE_IS_OPENED = "//div/h1[@class='product-title']";
    String CLICK_ON_ITEM = "//a[@href='http://192.168.214.3:8080/products/-/product/vegetables']";
    String CLICK_ON_HEADING = "//h3/a[@class='list_href']";
    String CLICK_ON_ITEM_ON_BREADCRUMB = "//a[@href='http://192.168.214.3:8080/products/-/category/vegetables']";
    String CLICK_ON_GRID_LIST = "//a[@href='http://192.168.214.3:8080/products/-/product/asc/orderValue/1/vegetables/1/12/grid']";
    String CLICK_ON_VIEW_LIST = "//a[@href='http://192.168.214.3:8080/products/-/product/asc/orderValue/1/vegetables/1/12/list']";
    String THE_ELEMENT_SHOULD_BE_PRESENTED = "//h3/span[contains(text(),'Vegetables')]";
    String CLICK_LAST_BUTTON = "//div[1]/*/ul/li/a[contains(text(),'Last')]";
    String CLICK_FIRST_BUTTON = "//div[1]/*/ul/li/a[contains(text(),'First')]";
    String CLICK_NEXT_BUTTON = "//div[1]/*/ul/li/a[contains(text(),'Next')]";
    String CLICK_PREVIOUS_BUTTON = "//div[1]/*/ul/li/a[contains(text(),'Previous')]";
    String MESSAGE_APPEARED = "//div[1]/*/ul/li/a[contains(text(),'Last')]";
    String LINK_ON_NET = ".//div[3]/*/span/a";
}
