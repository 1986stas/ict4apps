package cucumberDemoProject2.steps.serenity;

/**
 * Created by user on 06.05.16.
 */
public interface LocatorsForSystemRegistration {
    String CLICK_ON_SING_IN = "//div/a[@class='login']";
    String CLICK_ON_CREATE_ACCOUNT = "//a[@id='_58_mefv_null_null']";
    String CLICK_ON_BUTTON_SAVE = "//button[@class='btn btn-primary']";
    String MESSAGE_SHOULD_APPEARED ="//div[@class='required']";
    String THE_MESSAGE_SHOULD_BE_APPEARED = "//div[@class='email']";

}
