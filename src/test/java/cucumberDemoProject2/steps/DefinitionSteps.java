package cucumberDemoProject2.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberDemoProject2.steps.serenity.EndUserSteps;
import net.thucydides.core.annotations.Steps;

public class DefinitionSteps {

    @Steps
    EndUserSteps steps;

    // Test case 02.0001
    @Given("^Go to url  \"([^\"]*)\"$")
    public void goToUrl(String arg0) throws Throwable {
        steps.goToUrl(arg0);
    }

    @When("^Click on Products in site navigation menu in header$")
    public void clickOnProductsInSiteNavigationMenuInHeader() throws Throwable {
        steps.clickOnProductsInSiteNavigationMenuInHeader();
    }


    @Then("^The chosen item should be opened$")
    public void theChosenItemShouldBeOpened() throws Throwable {
        steps.theChosenItemShouldBeOpened();
    }

    //Test case 02.0002
    @Given("^Page \"([^\"]*)\" opened$")
    public void pageOpened(String arg0) throws Throwable {
        steps.pageOpened();
    }

    @When("^Click on Fruits in drop down menu$")
    public void clickOnFruitsInDropDownMenu() throws Throwable {
        steps.clickOnFruitsInDropDownMenu();
    }

    @And("^Click on Vegetables$")
    public void clickOnVegetables() throws Throwable {
        steps.clickOnVegetables();
    }


    @Then("^The chosen category of FRUITS should be opened$")
    public void theChosenCategoryOfFRUITSShouldBeOpened() throws Throwable {
        steps.theChosenCategoryOfFRUITSShouldBeOpened();
    }

    //Test case 02.0003
    @Given("^Category Vegetables opened$")
    public void categoryVegetablesOpened() throws Throwable {
        steps.categoryVegetablesOpened();
    }

    //Test case 02.0004
    @When("^Click on link \"([^\"]*)\"$")
    public void clickOnLink(String ReadMore) throws Throwable {
        steps.clickOnLink();
    }

    @Then("^The chosen product should be opened$")
    public void theChosenProductShouldBeOpened() throws Throwable {
        steps.theChosenProductShouldBeOpened();
    }
    //Test case 02.0005

    @Given("^Product page is opened$")
    public void productPageIsOpened() throws Throwable {
        steps.productPageIsOpened();
    }

    @When("^Click on image of product$")
    public void clickOnImageOfProduct() throws Throwable {
        steps.clickOnImageOfProduct();
    }

    @Then("^Click on close button$")
    public void clickOnCloseButton() throws Throwable {
        steps.clickOnCloseButton();
        //Test case 02.0006
    }

    @When("^Click on item \"([^\"]*)\"$")
    public void clickOnItem(String arg0) throws Throwable {
        steps.clickOnItem();
    }

    @Then("^Previous levels must be opened$")
    public void previousLevelsMustBeOpened() throws Throwable {
        steps.previousLevelsMustBeOpened();
    }

    //Test case 02.0007
    @When("^Click on heading of product$")
    public void clickOnHeadingOfProduct() throws Throwable {
        steps.clickOnHeadingOfProduct();
    }

    //Test case02.0008
    @When("^Click on item \"([^\"]*)\" on breadcrumb$")
    public void clickOnItemOnBreadcrumb(String arg0) throws Throwable {
        steps.clickOnItemOnBreadcrumb();
    }

    //Test case 02.0009
    @When("^Click on \"([^\"]*)\"$")
    public void clickOn(String ClickOnGridList) throws Throwable {
        steps.clickOn();
    }

    @And("^Click on View list$")
    public void clickOnViewList() throws Throwable {
        steps.clickOnViewList();
    }

    @Then("^The element should be presented$")
    public void theElementShouldBePresented() throws Throwable {
        steps.theElementShouldBePresented();
    }

    //Test case 02.0010
    //Test case 06.0001
    @Given("^Go to \"([^\"]*)\"$")
    public void goTo(String arg0) throws Throwable {
        steps.goTo(arg0);
    }

    @When("^Click on SING IN$")
    public void clickOnSINGIN() throws Throwable {
        steps.clickOnSINGIN();
    }

    @And("^Click on link Create Account$")
    public void clickOnLinkCreateAccount() throws Throwable {
        steps.clickOnLinkCreateAccount();
    }

    @And("^Click on button SAVE$")
    public void clickOnButtonSAVE() throws Throwable {
        steps.clickOnButtonSAVE();
    }

    @Then("^The messages \"([^\"]*)\" should be appeared$")
    public void theMessagesShouldBeAppeared(String arg0) throws Throwable {
        steps.theMessagesShouldBeAppeared();
    }


    @And("^The message \"([^\"]*)\" should be appeared$")
    public void theMessageShouldBeAppeared(String arg0) throws Throwable {
        steps.theMessageShouldBeAppeared();
    }

    //Test case 08.0003
    @Given("^Get \"([^\"]*)\"$")
    public void get(String WelcomePageOpened) throws Throwable {
        steps.get(WelcomePageOpened);
    }

    @When("^Click on ([^\"]*) in site navigation menu$")
    public void clickOnItemInSiteNavigationMenu(String clickOnItem) throws InterruptedException {
        steps.clickOnItemInSiteNavigationMenu(clickOnItem);
    }
    //Test case 06.0002

    @Given("^Entry field Email Address empty$")
    public void entryFieldEmailAddressEmpty() throws InterruptedException {
        steps.entryFieldEmailAddressEmpty();
    }



    @Then("^Input values on entry field Email Address$")
    public void inputValuesOnEntryFieldEmailAddress() throws Throwable {
       steps.inputValuesOnEntryFieldEmailAddress();
    }

    //Test case 08.0001 08.0002
    @When("^Click on link AppSTore$")
    public void clickOnLinkAppSTore() throws Throwable {
        steps.clickOnLinkAppSTore();
    }

    @And("^Click on link GooglePlay$")
    public void clickOnLinkGooglePlay() throws Throwable {
        steps.clickOnLinkGooglePlay();
    }

    @Then("^The Welcome page should be opened$")
    public void theWelcomePageShouldBeOpened() throws Throwable {
        steps.theWelcomePageShouldBeOpened();
    }
    // Test case 10.0001

    @When("^Check carousel-button \"([^\"]*)\"$")
    public void checkCarouselButton(String arg0) throws Throwable {
        steps.checkCarouselButton();
    }


    @And("^Click on carousel-button \"([^\"]*)\"$")
    public void clickOnCarouselButton(String arg0) throws Throwable {
        steps.clickOnCarouselButton();
    }

    @And("^Click on first images$")
    public void clickOnFirstImages() throws Throwable {
        steps.clickOnFirstImages();
    }

    @Then("^The chosen product of \"([^\"]*)\" box should be opened$")
    public void theChosenProductOfBoxShouldBeOpened(String arg0) throws Throwable {
        steps.theChosenProductOfBoxShouldBeOpened(arg0);
    }

    @And("^Click on last images$")
    public void clickOnLastImages() throws Throwable {
        steps.clickOnLastImages();
    }

    //Test case 11.0001
    @When("^Click on first images in FOOD DELIVERY BOXES$")
    public void clickOnFirstImagesInFOODDELIVERYBOXES()  {
        steps.clickOnFirstImagesInFOODDELIVERYBOXES();
    }

    @Then("^The chosen product FOOD DELIVERY BOXES should be opened$")
    public void theChosenProductFOODDELIVERYBOXESShouldBeOpened() throws Throwable {
        steps.theChosenProductFOODDELIVERYBOXESShouldBeOpened();
    }

    @When("^Click on last images in FOOD DELIVERY BOXES$")
    public void clickOnLastImagesInFOODDELIVERYBOXES() throws Throwable {
       steps.clickOnLastImagesInFOODDELIVERYBOXES();
    }
    //Test case 11.0004
    @When("^Click on button next$")
    public void clickOnButtonNext() throws Throwable {
        steps.clickOnButtonNext();
    }

    @And("^Click on button previous$")
    public void clickOnButtonPrevious() throws Throwable {
        steps.clickOnButtonPrevious();
    }

    @Then("^Images with products should be changed$")
    public void imagesWithProductsShouldBeChanged() throws Throwable {
        steps.imagesWithProductsShouldBeChanged();
    }
    //Test case 11.0005
    @When("^Click on link ReadMoreAboutUs$")
    public void clickOnLinkReadMoreAboutUs() throws Throwable {
        steps.clickOnLinkReadMoreAboutUs();
    }

    @Then("^Should be opening category \"([^\"]*)\"$")
    public void shouldBeOpeningCategory(String ReadMoreAboutUs) throws Throwable {
        steps.shouldBeOpeningCategory(ReadMoreAboutUs);
    }
    //Test case 09.0002
    @When("^Input complete request \"([^\"]*)\" in search query$")
    public void inputCompleteRequestInSearchQuery(String apple) throws Throwable {
        steps.inputCompleteRequestInSearchQuery(apple);
    }

    @Then("^The result of request in document search container should be appeared$")
    public void theResultOfRequestInDocumentSearchContainerShouldBeAppeared() throws Throwable {
        steps.theResultOfRequestInDocumentSearchContainerShouldBeAppeared();
    }


    //Test case 09.0003
    @When("^Enter complete request \"([^\"]*)\" in search query$")
    public void enterCompleteRequestInSearchQuery(String arg0) throws Throwable {
       steps.enterCompleteRequestInSearchQuery();
    }

    @And("^In ([^\"]*) select global$")
    public void inSelectedScopeSelectGlobal(String global) throws Throwable {
        steps.inSelectedScopeSelectGlobal(global);
    }

    //Test case 12.0001
    @When("^Click on heading on first news in blog-list$")
    public void clickOnHeadingOnFirstNewsInBlogList() throws Throwable {
        steps.clickOnHeadingOnFirstNewsInBlogList();
    }

    @And("^Click on link Read more$")
    public void clickOnLinkReadMore() throws Throwable {
        steps.clickOnLinkReadMore();
    }

    @Then("^Should be opened website, there located chosen post$")
    public void shouldBeOpenedWebsiteThereLocatedChosenPost() throws Throwable {
        steps.shouldBeOpenedWebsiteThereLocatedChosenPost();
    }
    //Test case 12.0004
    @Given("^The first post in BLOGS on WELCOME page is opened$")
    public void theFirstPostInBLOGSOnWELCOMEPageIsOpened() throws Throwable {
        steps.theFirstPostInBLOGSOnWELCOMEPageIsOpened();
    }

    @When("^Click on Twitter link$")
    public void clickOnTwitterLink() throws Throwable {
        steps.clickOnTwitterLink();
    }
    //Test case 13.0001
    @When("^Click on ([^\"]*) in footer$")
    public void clickOnFooterItemInFooter(String clickOnItem) throws Throwable {
        steps.clickOnFooterItemInFooter(clickOnItem);
    }

    //Test case 13.0006
    @When("^Scroll down to the footer and click on back top button$")
    public void scrollDownToTheFooterAndClickOnBackTopButton() throws Throwable {
        steps.scrollDownToTheFooterAndClickOnBackTopButton();
    }

    @Then("^The top of the page should be appeared$")
    public void theTopOfThePageShouldBeAppeared() throws Throwable {
        steps.theTopOfThePageShouldBeAppeared();
    }
    //Test case
    @When("^Click on the first phone number link$")
    public void clickOnTheFirstPhoneNumberLink() throws Throwable {
        steps.clickOnTheFirstPhoneNumberLink();
    }

    @Then("^The special page with telephone number should be opened$")
    public void theSpecialPageWithTelephoneNumberShouldBeOpened() throws Throwable {
        steps.theSpecialPageWithTelephoneNumberShouldBeOpened();
    }
    //Test case 02.0013
    @When("^Click on pagination button last$")
    public void clickOnPaginationButtonLast() throws Throwable {
        steps.clickOnPaginationButtonLast();
    }

    @And("^Click on pagination button first$")
    public void clickOnPaginationButtonFirst() throws Throwable {
        steps.clickOnPaginationButtonFirst();
    }

    @And("^Click on pagination button next$")
    public void clickOnPaginationButtonNext() throws Throwable {
        steps.clickOnPaginationButtonNext();
    }

    @And("^Click on pagination button previous$")
    public void clickOnPaginationButtonPrevious() throws Throwable {
        steps.clickOnPaginationButtonPrevious();
    }

    @Then("^The message appeared$")
    public void theMessageAppeared() throws Throwable {
       steps.theMessageAppeared();
    }
    //Test case 02.0017
    @When("^Click on ([^\"]*)$")
    public void clickOnSocialNet(String socialNet) throws Throwable {
        steps.clickOnSocialNet(socialNet);
    }

}