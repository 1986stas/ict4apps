package cucumberDemoProject2.pages;

import cucumberDemoProject2.steps.serenity.LocatorsForSystemRegistration;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


/**
 * Created by user on 06.05.16.
 */
public class SystemRegistration extends PageObject {


    public void clickOnSINGIN() {
        $(LocatorsForSystemRegistration.CLICK_ON_SING_IN).click();
    }

    public void clickOnLinkCreateAccount() {
        $(LocatorsForSystemRegistration.CLICK_ON_CREATE_ACCOUNT).click();
    }

    public void clickOnButtonSAVE() {
        $(LocatorsForSystemRegistration.CLICK_ON_BUTTON_SAVE).click();
    }

    public boolean theMessagesShouldBeAppeared() {
        return $(LocatorsForSystemRegistration.MESSAGE_SHOULD_APPEARED).isPresent();
    }

    public boolean theMessageShouldBeAppeared() {
        return $(LocatorsForSystemRegistration.THE_MESSAGE_SHOULD_BE_APPEARED).isPresent();
    }

    public boolean entryFieldEmailAddressEmpty() {
        WebElement isEmptyField = getDriver().findElement(By.xpath("//input[@id='_58_emailAddress']"));
        if (isEmptyField != null) {
            return true;
         }else{
        return false;
        }
    }


    public void inputValuesOnEntryFieldEmailAddress() {
        WebElement firstName = getDriver().findElement(By.xpath("//input[@id='_58_firstName']"));
        WebElement screenName = getDriver().findElement(By.xpath("//input[@id='_58_screenName']"));
        WebElement button = getDriver().findElement(By.xpath("//button[@class='btn btn-primary']"));
        firstName.sendKeys("misantropicDivision");
        screenName.sendKeys("fuckingRevolution");
        button.click();
    }
}
 /*
        if (getDriver().findElement(By.xpath("//input[@id='_58_emailAddress']")).getText().matches("^Entry field Email Address empty$")) {
            if (new String(isEmpty).equals("")) ;
            return true;
        } else {
            return false;
        }*/

