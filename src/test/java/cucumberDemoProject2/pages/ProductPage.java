package cucumberDemoProject2.pages;

import cucumberDemoProject2.steps.serenity.ILocators;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * Created by user on 28.04.16.
 */
public class  ProductPage extends PageObject {

    

    public void clickOnProductsInSiteNavigationMenuInHeader() {

        $(ILocators.CLICK_ON_ITEM_PRODUCTS).click();
    }

    public boolean theChosenItemShouldBeOpened() {

        return $(ILocators.PRODUCTS_HEADER_TITLE).isPresent();
    }


    public boolean pageOpened() {
        return $(ILocators.ITEM_PRODUCTS_OPENED).isPresent();
    }

    public void clickOnFruitsInDropDownMenu() {
        $(ILocators.CLICK_ON_FRUITS_IN_SITE_NAVIGATION_MENU).click();
    }

    public void clickOnVegetables() {
      $(ILocators.CLICK_ON_VEGETABLES).click();
   }


    public boolean theChosenCategoryOfFRUITSShouldBeOpened() {
      return  $(ILocators.ITEM_VEGETABLES_SHOULD_BE_OPENED).isPresent();

    }

    public boolean categoryVegetablesOpened() {

        return $(ILocators.CATEGORY_VEGETABLES_OPENED).isPresent();
    }

    public void clickOnLink() {
        $(ILocators.CLICK_ON_READMORE).click();
    }

    public boolean theChosenProductShouldBeOpened() {

        return $(ILocators.CHOSEN_PRODUCT_SHOULD_BE_OPENED).isPresent();
    }
    public void productPageIsOpened() {
        $(ILocators.PRODUCT_PAGE_IS_OPENED).isPresent();
    }
    public void clickOnImageOfProduct() {
        $(ILocators.CLICK_ON_IMAGE).click();
    }

    public void clickOnCloseButton() {
        $(ILocators.CLICK_ON_CLOSE_BUTTON).click();
    }

    public void clickOnItem() {
        $(ILocators.CLICK_ON_ITEM).click();
    }
    public boolean previousLevelsMustBeOpened() {
      return  $(ILocators.CATEGORY_VEGETABLES_OPENED).isPresent();
    }

    public void clickOnHeadingOfProduct() {
        $(ILocators.CLICK_ON_HEADING).click();
    }

    public void clickOnItemOnBreadcrumb() {
        $(ILocators.CLICK_ON_ITEM_ON_BREADCRUMB).click();
    }

    public void clickOn() {
        $(ILocators.CLICK_ON_GRID_LIST).click();
    }

    public void clickOnViewList() {
        $(ILocators.CLICK_ON_VIEW_LIST).click();
    }

    public void theElementShouldBePresented() {
        $(ILocators.THE_ELEMENT_SHOULD_BE_PRESENTED).isPresent();
    }

    public void clickOnPaginationButtonLast() {
        $(ILocators.CLICK_LAST_BUTTON).click();
    }

    public void clickOnPaginationButtonFirst() {
        $(ILocators.CLICK_FIRST_BUTTON).click();
    }

    public void clickOnPaginationButtonNext() {
        $(ILocators.CLICK_NEXT_BUTTON).click();
    }

    public void clickOnPaginationButtonPrevious() {
        $(ILocators.CLICK_PREVIOUS_BUTTON).click();
    }

    public void theMessageAppeared() {
        $(ILocators.MESSAGE_APPEARED).isVisible();
    }

    public boolean clickOnSocialNet(String socialNet) throws InterruptedException {
        List <WebElement> listOfItems = getDriver().findElements(By.xpath(".//div[3]/*/span/a"));
        List <WebElement> url = new ArrayList<WebElement>();
        WebDriverWait wait = new WebDriverWait(getDriver(), 1);
        String startUrl= "http://192.168.214.3:8080/products/-/category/vegetables";
        for (int i = 0; i<=listOfItems.size(); i++) {
            listOfItems = getDriver().findElements(By.xpath(".//div[3]/*/span/a"));
            getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            wait.until(ExpectedConditions.visibilityOf(listOfItems.get(i)));
            listOfItems.get(i).click();
            getDriver().getCurrentUrl();
            ////есть сомнения в корретности работы
            for (WebElement URL : url)
                if (URL.getText().equals(startUrl))
                    return false;
        }
                    return true;
    }

}


