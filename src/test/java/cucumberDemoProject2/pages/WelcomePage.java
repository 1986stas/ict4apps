package cucumberDemoProject2.pages;


import cucumberDemoProject2.steps.serenity.Path;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.Point;
import org.openqa.selenium.interactions.Actions;

import java.awt.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by user on 10.05.16.
 */
    @DefaultUrl("http://192.168.214.3:8080/welcome")
public class WelcomePage extends PageObject {
    @FindBy(xpath = cucumberDemoProject2.steps.serenity.Path.ITEMS_OF_SITE_NAVIGATION_MENU)

    private WebElementFacade itemSiteNavigationMenu;


    public String clickOnItemInSiteNavigationMenu(String clickOnItem)throws InterruptedException {
        List<WebElement> nameList = new ArrayList <WebElement>();
        String hrefItemMenu;
        int i=0;
        nameList=getDriver().findElements(By.xpath(".//*/nav/ul/li/a/span"));
        while(!nameList.get(i).getText().equals(clickOnItem)){
            i++;
        }
        i++;
        hrefItemMenu=$(".//*/nav/ul/li["+i+"]/a").getAttribute("href");
        getDriver().findElement(By.xpath(".//*/li["+i+"]/a")).click();
        return hrefItemMenu;
    }

    public void clickOnLinkAppSTore() throws AWTException {
        WebElement AppStore = getDriver().findElement(By.xpath("//a[@href='//itunes.apple.com/us/app/ict4apps-series-basic/id913949563?l=ru&ls=1&mt=8']"));
        AppStore.click();
        String parentHandle = getDriver().getWindowHandle();
        //   getDriver().findElement(By.xpath("//a[@href='//play.google.com/store/apps/details?id=com.ict4apps.demo']")).click();

        for (String winHandle : getDriver().getWindowHandles()) {
            getDriver().switchTo().window(winHandle);
        }
        getDriver().navigate().back();
        getDriver().switchTo().window(parentHandle);

        /*
        Robot robot = new Robot();
        waitABit(1000);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_W);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyRelease(KeyEvent.VK_W);
        getDriver().get("http://192.168.214.3:8080/welcome");
        */
    }

    public void clickOnLinkGooglePlay() {
        String parentHandle = getDriver().getWindowHandle();
        getDriver().findElement(By.xpath("//a[@href='//play.google.com/store/apps/details?id=com.ict4apps.demo']")).click();

        for (String winHandle : getDriver().getWindowHandles()) {
            getDriver().switchTo().window(winHandle);
        }
        getDriver().navigate().back();
        getDriver().switchTo().window(parentHandle);
    }

    public void theWelcomePageShouldBeOpened() {
        String URL = getDriver().getCurrentUrl();
        Assert.assertEquals(URL, "http://192.168.214.3:8080/welcome" );
    }

    public void checkCarouselButton() {
        getDriver().findElement(By.xpath("././/div[1]/button [@class='slick-next']")).click();
    }


    public void clickOnCarouselButton() {
        waitABit(3000);
        getDriver().findElement(By.xpath("././/div[1]/button [@class='slick-prev']")).click();
    }

    public void clickOnFirstImages() throws InterruptedException, AWTException {
        waitABit(2000);
        Point coordinates = getDriver().findElement(By.xpath("//div[@data-slick-index='0']")).getLocation();
        Robot robot = new Robot();
        WebElement SpecialOffersFirst = getDriver().findElement(By.xpath("//div[@data-slick-index='0']"));
        SpecialOffersFirst.click();
        robot.mouseMove(coordinates.x, coordinates.y + 900);

    }

    public boolean theChosenProductOfBoxShouldBeOpened(String arg0) {
        waitABit(2000);
       if ($(Path.ITEM_IS_PRESENT).isPresent());
        return true;
    }

    public void clickOnLastImages() throws AWTException {
        waitABit(3000);
        Point coordinates = getDriver().findElement(By.xpath("//div[@data-slick-index='4']")).getLocation();
        Robot robot = new Robot();
        WebElement SpecialOffersLast = getDriver().findElement(By.xpath("//div[@data-slick-index='4']"));
        SpecialOffersLast.click();
        robot.mouseMove(coordinates.x, coordinates.y + 900);
    }

    public void clickOnFirstImagesInFOODDELIVERYBOXES()  {
        Actions builder = new Actions(getDriver());
        WebElement firstImageClick = getDriver().findElement(By.xpath("//div[@class='item_content num_0']"));
        builder.moveToElement(firstImageClick).perform();
        builder.moveToElement(firstImageClick).click().perform();
    }

    public void theChosenProductFOODDELIVERYBOXESShouldBeOpened() {
        $(Path.ITEM_IS_PRESENT).isPresent();
    }

    public void clickOnLastImagesInFOODDELIVERYBOXES() {
      //  getDriver().findElement(By.xpath("//div[1]/ul/button[@class='slick-prev']")).click();
      //  WebElement draggable = getDriver().findElement(By.xpath("//div[@class='item_content num_8']"));
        Actions builder = new Actions(getDriver());
      //  WebElement draggable = getDriver().findElement(By.xpath("//div[@class='item_content num_8']"));
      //  builder.dragAndDropBy(draggable, 900, 0).perform();
        getDriver().findElement(By.xpath("//div[1]/ul/button[@class='slick-prev']")).click();
        waitFor(find(By.xpath("//div[@class='item_content num_8']")));
        WebElement firstImageClick = getDriver().findElement(By.xpath("//div[@class='item_content num_8']"));
        builder.moveToElement(firstImageClick).perform();
        builder.moveToElement(firstImageClick).click().perform();
    }

    public void clickOnButtonNext() {
        JavascriptExecutor jse = (JavascriptExecutor)getDriver();
        jse.executeScript("window.scrollBy(0,1000)", "");
        getDriver().findElement(By.xpath("//div[1]/ul/button[@class='slick-next']")).click();
    }

    public void clickOnButtonPrevious() {
        getDriver().findElement(By.xpath("//div[1]/ul/button[@class='slick-prev']")).click();
    }

    public void imagesWithProductsShouldBeChanged() {
        $(Path.THE_PRODUCT_SHOULD_BE_CHANGED).isDisplayed();
    }

    public void clickOnLinkReadMoreAboutUs() {
        $(Path.LINK_READ_MORE_ABOUT_US).click();
    }

    public void shouldBeOpeningCategory(String readMoreAboutUs) {
        $(Path.LINK_SHOULD_BE_CONTAINS).isPresent();
    }

    public void inputCompleteRequestInSearchQuery(String apple) {
        WebElement request = getDriver().findElement(By.xpath("//input[@id='yui_patched_v3_11_0_1_1430216317928_790']"));
        WebElement button = getDriver().findElement(By.xpath("//button[@id='yui_patched_v3_11_0_1_1429887029852_855']"));
        request.sendKeys("apple");
        button.click();
    }

    public String theResultOfRequestInDocumentSearchContainerShouldBeAppeared() {
        if(getDriver().getPageSource().contains("apple"))
        {return ("passed");}
        else
        {return ("failed");}
    }
        /*
        Actions builder = new Actions(getDriver());
        WebElement firstImageClick = getDriver().findElement(By.xpath("//div/select[@id='yui_patched_v3_11_0_1_1463487140017_429']"));
        builder.moveToElement(firstImageClick).perform();
        builder.moveToElement(firstImageClick).click().perform();
        Select selectedScope = new Select(getDriver().findElement(By.xpath("//option[@value='global']")));
        selectedScope.selectByVisibleText("Global");

        Actions action = new Actions(getDriver());
        WebElement we = getDriver().findElement(By.xpath("//select[@id='yui_patched_v3_11_0_1_1463487140017_429']"));
        action.moveToElement(we).moveToElement(getDriver().findElement(By.xpath("//select[@id='yui_patched_v3_11_0_1_1463487140017_429']"))).click().build().perform();

        waitABit(2000);
        Actions builder = new Actions(getDriver());
        WebElement draggable = getDriver().findElement(By.xpath("//div[@data-slick-index='0']"));
        builder.dragAndDropBy(draggable, 1000, 0).perform();
        builder.moveToElement((WebElement) By.xpath("//div[@data-slick-index='0']")).perform();
        builder.moveToElement((WebElement) By.xpath("//div[@data-slick-index='0']")).click().perform();
        }

        public void clickOnTitlesOfProduct() throws InterruptedException {
        waitABit(5000);
        Actions builder = new Actions(getDriver());
        WebElement draggable = getDriver().findElement(By.xpath(""));
        builder.dragAndDropBy(draggable, 900, 0).perform();

        List<WebElement> selectScope = new ArrayList <WebElement>();
        String select;
        int i=0;
  //      selectScope = getDriver().findElements(By.xpath(".///fieldset/div/select/option"));
   //         while(!selectScope.get(i).getText().equals(global)){
  //           i++;
   //       }
  //           i++;
  //      select=$(".///fieldset/div/select["+i+"]/option").getAttribute("option");
  //      getDriver().findElement(By.xpath(".//*///fieldset/div/select["+i+"]/option")).click();
  //      WebElement searchButton = find(By.xpath("//button[@id='_3_search']"));
  //      searchButton.click();
   //     return select;

  //     builder.dragAndDrop(draggable, 900,0).perform();
  //     builder.moveToElement((WebElement) By.xpath("")).perform();
  //     builder.moveToElement((WebElement) By.xpath("")).click().perform();

    //       String valueToSelect= "Global";
//        WebElement select = getDriver().findElement(By.xpath(".//*/fieldset/div/select/option"));
//        WebElement searchButton = find(By.xpath("//button[@id='_3_search']"));
//        Select dropDown = new Select(select);
//        String selected = dropDown.getFirstSelectedOption().getText();
//        if(selected.equals(valueToSelect)) {
//            List<WebElement> Options = dropDown.getOptions();
//            for (WebElement option : Options) {
//                if (option.getText().equals(valueToSelect)) {
//                    option.click();
//                    searchButton.click();
//                }
//            }
//        }

    public void enterCompleteRequestInSearchQuery() {
        WebElement request = getDriver().findElement(By.xpath("//input[@type='text']"));
        request.sendKeys("apple");
    }

    public void inSelectedScopeSelectGlobal(String global) {
        Actions action = new Actions(getDriver());
        WebElement we = getDriver().findElement(By.xpath("//option[@value='"+global+"']"));
        action.click(we).perform();
        WebElement button = getDriver().findElement(By.xpath("//button[@id='_3_search']"));
        button.click();
    }

    public void clickOnHeadingOnFirstNewsInBlogList() {
        $(Path.CLICK_ON_FIRST_NEWS).click();
    }

    public void clickOnLinkReadMore() {
        $(Path.CLICK_ON_READ_MORE).click();
    }

    public boolean shouldBeOpenedWebsiteThereLocatedChosenPost() {
        String URL = getDriver().getCurrentUrl();
        if (URL != "http://192.168.214.3:8080/welcome" );
        return true;
    }

    public void theFirstPostInBLOGSOnWELCOMEPageIsOpened() {
        $(Path.CLICK_ON_FIRST_NEWS).click();
    }
    @Deprecated
    public void clickOnTwitterLink()  {
        $(Path.CLICK_ON_TWITTER_LINK).click();
        String parent = getDriver().getWindowHandle();
        Set<String> popup = getDriver().getWindowHandles();
            Iterator<String> twitter = popup.iterator();
            while (twitter.hasNext()) {
                String popupHandle=twitter.next().toString();
                if(!popupHandle.contains(parent))
                 {   Set<String> beforePopup = getDriver().getWindowHandles();
                     Set<String> afterPopup = getDriver().getWindowHandles();
                     afterPopup.removeAll(beforePopup);
                     if(afterPopup.size() == 1)
                     {
                         getDriver().switchTo().window((String)afterPopup.toArray()[0]);
                     }
                    getDriver().switchTo().window(popupHandle);
                    getDriver().close();
                     }
                }
    }

    public String clickOnFooterItemInFooter(String clickOnItem) {
        List<WebElement> footerList = new ArrayList <WebElement>();
        String footerItemMenu;
        int i=0;
        footerList=getDriver().findElements(By.xpath("//footer//div[1]/ul/li/a/span"));
        while(!footerList.get(i).getText().equals(clickOnItem)){
            i++;
        }
        i++;
        footerItemMenu = $("//footer//div[1]/ul/li["+i+"]/a").getAttribute("href");
        getDriver().findElement(By.xpath("//footer//div[1]/ul/li["+i+"]/a")).click();
        return footerItemMenu;
    }

    public void scrollDownToTheFooterAndClickOnBackTopButton() {
        JavascriptExecutor jse = (JavascriptExecutor)getDriver();
        jse.executeScript("window.scrollBy(0,1400)", "");
        getDriver().findElement(By.xpath("//div/a[@href='#']")).click();
    }

    public void theTopOfThePageShouldBeAppeared() {
        $(Path.ELEMENT_IS_PRESENT).isDisplayed();
    }

    public void clickOnTheFirstPhoneNumberLink() {
        $(Path.CLICK_ON_NUMBER).isDisplayed();
    }

    public void theSpecialPageWithTelephoneNumberShouldBeOpened() {
        $(Path.PHONE_NUMBER_IS_ENABLED).isEnabled();
    }


}
